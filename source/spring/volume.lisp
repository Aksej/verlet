(in-package #:verlet)

(defsection @volume-spring (:title "Volume Springs")
  "Volume springs are hard to predict and may be buggy in unknown
 ways. Combining them with standard or area springs seems to violate
 conservation of momentum."
  (volume-spring class)
  ([volume-spring] function)
  (volume<- (accessor volume-spring))
  (k<- (accessor volume-spring))
  (update! (method () (volume-spring))))

(defclass volume-spring ()
  ((%p1 :type particle
        :initarg :p1
        :reader p1<-)
   (%p2 :type particle
        :initarg :p2
        :reader p2<-)
   (%p3 :type particle
        :initarg :p3
        :reader p3<-)
   (%p4 :type particle
        :initarg :p4
        :reader p4<-)
   (%volume :type (single-float 0.0)
            :initarg :volume
            :accessor volume<-)
   (%k :type (single-float (0.0))
       :initarg :k
       :accessor k<-)))

(defun [volume-spring] (p1 p2 p3 p4 k
                        &key
                          (volume
                           (abs (/ (v3:dot (v3:- (position<- p4)
                                                 (position<- p1))
                                           (v3:cross (v3:- (position<- p3)
                                                           (position<- p1))
                                                     (v3:- (position<- p2)
                                                           (position<- p1))))
                                   6))))
  "Create a VOLUME-SPRING with corners P1, P2, P3 and P4, ideal volume VOLUME,
 and spring constant K. VOLUME defaults to the volume enclosed by the given
 particles."
  (make-instance 'volume-spring
                 :p1 p1
                 :p2 p2
                 :p3 p3
                 :p4 p4
                 :volume volume
                 :k k))

(defmethod update! ((object volume-spring)
                    &optional
                      delta)
  "Exert a volume-restoring force on the corners of the volume spring."
  (declare (ignore delta))
  (bind ((p1* (p1<- object))
         (p2* (p2<- object))
         (p3* (p3<- object))
         (p4* (p4<- object))
         (p1 (position<- p1*))
         (p2 (position<- p2*))
         (p3 (position<- p3*))
         (p4 (position<- p4*))
         (m1 (mass<- p1*))
         (m2 (mass<- p2*))
         (m3 (mass<- p3*))
         (m4 (mass<- p4*))
         (center (v3:/s (v3:+ (v3:*s p1 m1)
                              (v3:*s p2 m2)
                              (v3:*s p3 m3)
                              (v3:*s p4 m4))
                        (+ m1 m2 m3 m4)))
         (volume-difference (- (volume<- object)
                               (abs (/ (v3:dot (v3:- p4 p1)
                                               (v3:cross (v3:- p3 p1)
                                                         (v3:- p2 p1)))
                                       6))))
         (f1 (* (v3:distance p1 center)
                m1))
         (f2 (* (v3:distance p2 center)
                m2))
         (f3 (* (v3:distance p3 center)
                m3))
         (f4 (* (v3:distance p4 center)
                m4))
         (f (/ (* (float-sign volume-difference)
                  (expt (abs volume-difference)
                        1/3)
                  (k<- object))
               (+ f1 f2 f3 f4))))
    (apply-force! p1* (v3:*s (v3:normalize (v3:- p1 center))
                             (* f1 f)))
    (apply-force! p2* (v3:*s (v3:normalize (v3:- p2 center))
                             (* f2 f)))
    (apply-force! p3* (v3:*s (v3:normalize (v3:- p3 center))
                             (* f3 f)))
    (apply-force! p4* (v3:*s (v3:normalize (v3:- p4 center))
                             (* f4 f)))))
