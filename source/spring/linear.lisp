(in-package #:verlet)

(defsection @spring (:title "Springs")
  (spring force)
  (@linear-spring section)
  (@area-spring section)
  (@volume-spring section))

(defsection @linear-spring (:title "Linear Springs")
  (spring class)
  ([spring] function)
  (k<- (accessor spring))
  (length<- (accessor spring))
  (update! (method () (spring))))

(defclass spring ()
  ((%p1 :type particle
        :initarg :p1
        :reader p1<-)
   (%p2 :type particle
        :initarg :p2
        :reader p2<-)
   (%k :type (single-float (0.0))
       :initarg :k
       :accessor k<-)
   (%length :type (single-float 0.0)
            :initarg :length
            :accessor length<-)))

(defun [spring] (p1 p2 k
                 &key
                   (length (v3:distance (position<- p1)
                                        (position<- p2))))
  "Create a [SPRING][class] connecting the `PARTICLE`s P1 and P2, with ideal
 length LENGTH and spring constant K. LENGTH defaults to the distance between P1
 and P2."
  (make-instance 'spring
                 :p1 p1
                 :p2 p2
                 :k k
                 :length length))

(defmethod update! ((object spring)
                    &optional
                      delta)
  "Exert a length-restoring force on the endpoints of the linear spring."
  (declare (ignore delta))
  (bind ((p1 (p1<- object))
         (p2 (p2<- object))
         (direction (v3:- (position<- p1)
                          (position<- p2)))
         (distance (v3:length direction))
         (direction (v3:*s direction (/ distance)))
         (force (/ (* (k<- object)
                      (- (length<- object)
                         distance))
                   2.0)))
    (apply-force! p1 (v3:*s direction force))
    (apply-force! p2 (v3:*s direction (- force)))))
