(in-package #:verlet)

(defsection @area-spring (:title "Area Springs")
  "Area springs are hard to predict and may be buggy in unknown ways. Combining
 them with standard or volume springs seems to violato conservation of
 momentum."
  (area-spring class)
  ([area-spring] function)
  (area<- (accessor area-spring))
  (k<- (accessor area-spring))
  (update! (method () (area-spring))))

(defclass area-spring ()
  ((%p1 :type particle
        :initarg :p1
        :reader p1<-)
   (%p2 :type particle
        :initarg :p2
        :reader p2<-)
   (%p3 :type particle
        :initarg :p3
        :reader p3<-)
   (%k :type (single-float (0.0))
       :initarg :k
       :accessor k<-)
   (%area :type (single-float 0.0)
          :initarg :area
          :accessor area<-)))

(defun [area-spring] (p1 p2 p3 k
                      &key
                        (area (/ (v3:length (v3:cross (v3:- (position<- p1)
                                                            (position<- p2))
                                                      (v3:- (position<- p1)
                                                            (position<- p3))))
                                 2.0)))
  "Creates an AREA-SPRING with corners P1, P2 and P3, ideal area AREA, and
 spring constant K. AREA defaults to the area enclosed by the supplied
 particles."
  (make-instance 'area-spring
                 :p1 p1
                 :p2 p2
                 :p3 p3
                 :k k
                 :area area))

(defmethod update! ((object area-spring)
                    &optional
                      (delta 1.0))
  "Exert an area-restoring force on the corners of the area spring"
  (declare (ignore delta))
  (bind ((p1* (p1<- object))
         (p2* (p2<- object))
         (p3* (p3<- object))
         (p1 (position<- p1*))
         (p2 (position<- p2*))
         (p3 (position<- p3*))
         (m1 (mass<- p1*))
         (m2 (mass<- p2*))
         (m3 (mass<- p3*))
         (center (v3:/s (v3:+ (v3:*s p1 m1)
                              (v3:*s p2 m2)
                              (v3:*s p3 m3))
                        (+ m1 m2 m3)))
         (area-difference (- (area<- object)
                             (/ (v3:length (v3:cross (v3:- p1 p2)
                                                     (v3:- p1 p3)))
                                2.0)))
         (f1 (* (v3:distance p1 center)
                m1))
         (f2 (* (v3:distance p2 center)
                m2))
         (f3 (* (v3:distance p3 center)
                m3))
         (f (/ (* (float-sign area-difference)
                  (sqrt (abs area-difference))
                  (k<- object))
               (+ f1 f2 f3))))
    (apply-force! p1* (v3:*s (v3:normalize (v3:- p1 center))
                             (* f1 f)))
    (apply-force! p2* (v3:*s (v3:normalize (v3:- p2 center))
                             (* f2 f)))
    (apply-force! p3* (v3:*s (v3:normalize (v3:- p3 center))
                             (* f3 f)))))
