(in-package #:verlet)

(defsection @force (:title "Force")
  (deforce macro)
  (force locative))

(bind ((force-register (make-hash-table :test 'eq)))
  (defun register-force (name mode lambda-list body
                         &optional
                           (docstring ""))
    (assert (member mode '(:force :velocity-dependent-force :constraint
                           :collision)
                    :test #'eq))
    (setf (gethash name force-register)
          (list mode lambda-list body docstring)))
  (defun find-force (name)
    (values-list (or (gethash name force-register)
                     (error "Tried to load unknown force ~A"
                            name)))))

(define-symbol-locative-type force ())

(defmacro deforce (name mode (&rest lambda-list)
                   &body
                     body)
  "Define a force so it is usable in the BUILD-SYSTEM macro.

 MODE is required to be one of :FORCE, :VELOCITY-DEPENDENT-FORCE, :CONSTRAINT,
 and :COLLISION. Reference the STEP! function for more information on what these
 mean.

 The LAMBDA-LIST is tha lambda list used by the local macro in the BUILD-SYSTEM
 macro call, BODY is it's body. The resulting code should return the object to
 add to the system under construction. PARTICLES is an anaphoric variable in the
 BODY, containing a list of the particles defined by PARTICLES in the
 BUILD-SYSTEM macro, in the order they are defined in."
  (bind ((docstring (if (and (rest body)
                             (stringp (first body)))
                        (first body)
                        ""))
         (body (if (and (rest body)
                        (stringp (first body)))
                   (rest body)
                   body)))
    `(progn
       (defmethod mgl-pax::symbol-lambda-list ((symbol (eql ',name))
                                               (locative-type (eql 'force)))
         ,docstring '
         ,lambda-list)
       (eval-when (:compile-toplevel :load-toplevel :execute)
         (register-force ',name ',mode ',lambda-list ',body ,docstring)))))
