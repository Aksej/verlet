(in-package #:verlet)

(defsection @plane-constraint (:title "Plane Constraints")
  (plane-constraint force)
  (plane-constraint class)
  ([plane-constraint] function)
  (violated? (method () (plane-constraint particle)))
  (deflection-vector (method () (plane-constraint particle))))

(defclass plane-constraint (constraint)
  ((%point :type (simple-array single-float (3))
           :initarg :point
           :reader point<-)
   (%normal :type (simple-array single-float (3))
            :initarg :normal
            :reader normal<-)))

(defun [plane-constraint] (point normal)
  "Creates a [PLANE-CONSTRAINT][class] restricting all subjects to a half-space
 defined by the plane containing POINT with normal NORMAL. The usable half-space
 is the closed half-space that NORMAL is pointing into."
  (make-instance 'plane-constraint
                 :point point
                 :normal (v3:normalize normal)))

(defmethod violated? ((constraint plane-constraint)
                      (particle particle))
  (< (v3:dot (v3:- (position<- particle)
                   (point<- constraint))
             (normal<- constraint))
     0.0))

(defmethod deflection-vector ((constraint plane-constraint)
                              (particle particle))
  (v3:*s (normal<- constraint)
         (v3:dot (v3:- (point<- constraint)
                       (position<- particle))
                 (normal<- constraint))))

(deforce plane-constraint :constraint ((x y z)
                                       (nx ny nz))
  `(bind ((con ([plane-constraint] (v! ,x ,y ,z)
                                   (v! ,nx ,ny ,nz))))
     (setf (subjects<- con)
           (convert 'set particles))
     con))
