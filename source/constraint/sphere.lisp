(in-package #:verlet)

(defsection @sphere-constraint (:title "Sphere Constraints")
  (sphere-constraint force)
  (sphere-constraint class)
  ([sphere-constraint] function)
  (violated? (method () (sphere-constraint particle)))
  (deflection-vector (method () (sphere-constraint particle))))

(defclass sphere-constraint (constraint)
  ((%center :type (simple-array single-float (3))
            :initarg :center
            :reader center<-)
   (%radius :type (single-float (0.0))
           :initarg :radius
           :reader radius<-)
   (%outer-wall? :type boolean
                 :initarg :outer-wall?
                 :reader outer-wall?)))

(defun [sphere-constraint] (center radius
                            &optional
                              outer-wall?)
  "Creates a [SPHERE-CONSTRAINT][class] with center CENTER and radius
 RADIUS. CENTER is a `(SIMPLE-ARRAY SINGLE-FLOAT (3))`, RADIUS a
 SINGLE-FLOAT. When OUTER-WALL? is truthy, movement is restricted to the closed
 ball described by the sphere, otherwise movement is constricted to the outside
 of the open ball described by the sphere."
  (make-instance 'sphere-constraint
                 :center center
                 :radius radius
                 :outer-wall? outer-wall?))

(defmethod violated? ((constraint sphere-constraint)
                      (particle particle))
  (funcall (if (outer-wall? constraint)
               #'>
               #'<)
           (v3:length (v3:- (center<- constraint)
                            (position<- particle)))
           (radius<- constraint)))

(defmethod deflection-vector ((constraint sphere-constraint)
                              (particle particle))
  (=> (:var dir (v3:- (position<- particle)
                      (center<- constraint)))
    (:var mag (v3:length %))
    (v3:normalize dir)
    (v3:*s % (- (radius<- constraint)
                mag))
    (if (outer-wall? constraint)
        (v3:negate %)
        %)))

(deforce sphere-constraint :constraint ((cx cy cz)
                                        r &optional outer-wall?)
  `(bind ((con ([sphere-constraint] (v! ,cx ,cy ,cz)
                                    (float ,r 0.0)
                                    ,outer-wall?)))
     (setf (subjects<- con)
           (convert 'set particles))
     con))
