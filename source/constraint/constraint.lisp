(in-package #:verlet)

(defsection @constraint (:title "Constraints")
  (constraint class)
  (violated? generic-function)
  (deflection-vector generic-function)
  (update! (method () (constraint)))
  (@plane-constraint section)
  (@sphere-constraint section)
  (@triangle-constraint section))

(defclass constraint (modificator)
  ())

(defgeneric violated? (constraint particle)
  (:documentation
   "Returns NIL when the constraint is fulfilled and a truthy value
 otherwise."))

(defgeneric deflection-vector (constraint particle)
  (:documentation
   "Returns the cange in position for PARTICLE to obey CONSTRAINT again."))

(defmethod update! ((constraint constraint)
                    &optional
                      delta)
  "Deflects all subjects violating the constraint as per VIOLATED? by the vector
 defined by DEFLECTION-VECTOR."
  (declare (ignore delta))
  (do-set (particle (subjects<- constraint))
    (when (violated? constraint particle)
      (deflect! particle (deflection-vector constraint particle)))))
