(in-package #:verlet)

(defsection @gravity (:title "Gravity")
  (@global-gravity section)
  (@mutual-gravity section)
  (@field-gravity section))
