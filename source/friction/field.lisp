(in-package #:verlet)

(defsection @field-friction (:title "Field Friction")
  (field-friction force)
  (field-friction class)
  ([field-friction] function)
  (field<- (accessor field-friction)))

(defclass field-friction (modificator)
  ((%field :type (function ((simple-array single-float (3)))
                           (single-float 0.0))
           :initarg :field
           :accessor field<-)))

(defun [field-friction] (field)
  "Creates [FIELD-FRICTION][class] with friction field FIELD. FIELD must map 3D
positions to friction values."
  (make-instance 'field-friction
                 :field field))

(defmethod update! ((object field-friction)
                    &optional
                      (delta 1.0))
  (bind ((field (field<- object)))
    (do-set (particle (subjects<- object))
      (friction! particle (funcall field (position<- particle))
                 delta))))

(deforce field-friction :velocity-dependent-force (position-var &body body)
  `(bind ((fr ([field-friction] (lambda (,position-var)
                                  ,@body))))
     (setf (subjects<- fr)
           (convert 'set particles))
     fr))
